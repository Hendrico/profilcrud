const express = require('express')
const Profil = require('../models/profil');
const router = express.Router()

// all routes
router.get('/', (req,res)=>{
    res.send("Route profil")
})

router.post('/create', async(req,res)=>{ 
    const newProfil = new Profil(req.body); 
    const savedProfil = await newProfil.save() 
    res.json(savedProfil) 
})

router.get('/:email', async (req, res) => {
    const profil = await Profil.findOne({ email: req.params.email });
    res.json(profil);
});

router.put('/:email', async (req, res) => {
    await Profil.updateOne({email: req.params.email}, {$set: req.body});
    const profil = await Profil.findOne({ email: req.params.email });
    res.json(profil);
});

router.delete('/:email', async (req, res) => {
    const result = await Profil.findOneAndDelete({email: req.params.email});
    res.json(result);
});

module.exports = router;