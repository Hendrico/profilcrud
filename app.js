const express = require("express")
const mongoose = require("mongoose")
const bodyParser = require("body-parser")
const profilRoute = require('./routes/profil')

// express app
const app = express()
// middleware
app.use(bodyParser.json())
// database
const uri = "mongodb+srv://admin:pP38Ge9OG97MKglc@cluster0.8ll8a.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Berhasil terkoneksi dengan MongoDB')
  })
  .catch(err => console.log(err))

// route
app.use('/profil', profilRoute)
app.get("/", (req,res)=>{
    res.send("halaman utama")
})

// memulai server
app.listen(3000, ()=>{
    console.log("server sudah menyala pada port 3000")
}) 