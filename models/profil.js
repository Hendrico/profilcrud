const mongoose = require('mongoose')
const ProfilsSchema = new mongoose.Schema({
    nama_lengkap: { type: String },
    nomor_telepon: { type: String },
    email: { type: String, unique: true }
})
module.exports = mongoose.model('profil', ProfilsSchema)